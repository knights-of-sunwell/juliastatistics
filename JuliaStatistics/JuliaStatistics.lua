
----------------------------------------------------------------------------
--         JuliaStatistics
--     Copyright (C) 2019  Knights of Sunwell
-- 
--     This program is free software: you can redistribute it and/or modify
--     it under the terms of the GNU General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.
-- 
--     This program is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU General Public License for more details.
-- 
--     You should have received a copy of the GNU General Public License
--     along with this program.  If not, see <http://www.gnu.org/licenses/>.
--     
-----------------------------------------------------------------------------
        

JS = {
	Revision = ("$Revision: 0 $"):sub(12, -3),
	Version = "1.1",
	DisplayVersion = "1.1"
}

local playerName = UnitName("player")


JS.Options = { minimapIcon = true,
               share       = true,
               minimapPos  = {x = -80, 
                              y = -40}}

JS.Data = {
    ["Gratz"]     = 0,
    ["Jackpot"]   = 0,
    ["Honor"]     = 0,
    ["Arena"]     = 0,
    ["Badge"]     = 0,
    ["Token"]     = 0,
    ["Orb"]       = 0,
    ["LiliRune"]  = 0,
    ["HonorJ"]    = 0,
    ["ArenaJ"]    = 0,
    ["BadgeJ"]    = 0,
    ["TokenJ"]    = 0,
    ["OrbJ"]      = 0,
    ["SirusRune"] = 0,
    ["Bd"]        = 0
}

JS.IDs = {
    ["Gratz"]     = 101000,
    ["Jackpot"]   = 89373,
    ["Honor"]     = 150603,
    ["Arena"]     = 74034,
    ["Badge"]     = 160000,
    ["Token"]     = 280512,
    ["Orb"]       = 43102,
    ["LiliRune"]  = 99998,
    ["HonorJ"]    = 150603,
    ["ArenaJ"]    = 74034,
    ["BadgeJ"]    = 160000,
    ["TokenJ"]    = 280512,
    ["OrbJ"]      = 23572,
    ["SirusRune"] = 100000,
    ["Bd"]        = 280505
}

JS.Amount = {
    ["Gratz"]     = nil,
    ["Jackpot"]   = nil,
    ["Honor"]     = 100,
    ["Arena"]     = 25,
    ["Badge"]     = 50,
    ["Token"]     = 3,
    ["Orb"]       = 1,
    ["LiliRune"]  = 1,
    ["HonorJ"]    = 500,
    ["ArenaJ"]    = 125,
    ["BadgeJ"]    = 250,
    ["TokenJ"]    = 9,
    ["OrbJ"]      = 1,
    ["SirusRune"] = 1,
    ["Bd"]        = 1
}

JS.LastWin = {
    ["Gratz"]     = "none",
    ["Jackpot"]   = "none",
    ["Honor"]     = "none",
    ["Arena"]     = "none",
    ["Badge"]     = "none",
    ["Token"]     = "none",
    ["Orb"]       = "none",
    ["LiliRune"]  = "none",
    ["HonorJ"]    = "none",
    ["ArenaJ"]    = "none",
    ["BadgeJ"]    = "none",
    ["TokenJ"]    = "none",
    ["OrbJ"]      = "none",
    ["SirusRune"] = "none",
    ["Bd"]        = "none"
}

JS.Synced = {}

JS.Order = {"Gratz","Jackpot","Honor","Arena","Badge","Token","Orb","LiliRune","HonorJ","ArenaJ","BadgeJ","TokenJ","OrbJ","SirusRune","Bd"}

JS.Users = {}

local backdrop = {
  bgFile = "Interface\\AddOns\\JuliaStatistics\\img\\background", tile = true, tileSize = 8,
  edgeFile = "Interface\\AddOns\\JuliaStatistics\\img\\border", edgeSize = 8,
  insets = {left = 0, right = 0, top = 0, bottom = 0},
}

local backdrop_noborder = {
  bgFile = "Interface\\AddOns\\JuliaStatistics\\img\\background", tile = true, tileSize = 8,
  insets = {left = 0, right = 0, top = 0, bottom = 0},
}

-----------------------
--Auxillary functions--
-----------------------
local function makeTexture(frame, path, blend)
    local t = frame:CreateTexture()
    t:SetAllPoints(frame)
    t:SetTexture(path)
    if blend then
        t:SetBlendMode(blend)
    end
    return t
end

local function summary(tablE)
    local sum = 0
    if tablE then
        for k,v in pairs(tablE) do
            if type(v) == "table" and  k ~= "Gratz" and k ~= "Jackpot" then
                sum = sum + summary(v)
            elseif k ~= "Gratz" and k ~= "Jackpot" then
            sum = sum + v
            end
        end
    end
    return sum
end

local function GetHash(tablE)
    local x = new256()
    for k, v in ipairs(JS.Order) do
        if type(tablE[v]) == "table" then
            local tOrder = {}
            for i, j in pairs(tablE[v]) do table.insert(tOrder, i) end
            table.sort(tOrder)
            for p, q in ipairs(tOrder) do
                q:gsub(".", function(c)
                    x:add(c)
                end)
                x:add("" .. tablE[v][q])
            end
        else
            x:add("" .. v)
        end
    end
    x:add('\n')
    return x:close()
end

local function SaveData()
    JSBase[playerName] = JS.Data
    JSUsers = JS.Users
    JSOptions = JS.Options
    JSLastWin = JS.LastWin
end

local function LoadData()
    JS.Data = JSBase[playerName] or JS.Data
    JS.Users = JSUsers or JS.Users
    JS.Options = JSOptions or JS.Options
    JS.LastWin = JSLastWin or JS.LastWin
end

local function UpdateSum(who)
    JS.Users["Gratz"][who]   = (JS.Users["Honor"][who] or 0) + (JS.Users["Arena"][who] or 0) + (JS.Users["Badge"][who] or 0) + (JS.Users["Token"][who] or 0) + (JS.Users["Orb"][who] or 0) + (JS.Users["LiliRune"][who] or 0)
    if JS.Users["Gratz"][who] == 0 then JS.Users["Gratz"][who] = nil end
    JS.Users["Jackpot"][who] = (JS.Users["HonorJ"][who] or 0) + (JS.Users["ArenaJ"][who] or 0) + (JS.Users["BadgeJ"][who] or 0) + (JS.Users["TokenJ"][who] or 0) + (JS.Users["OrbJ"][who] or 0) + (JS.Users["SirusRune"][who] or 0) + (JS.Users["Bd"][who] or 0)
    if JS.Users["Jackpot"][who] == 0 then JS.Users["Jackpot"][who] = nil end
end

local function ClearData()
    for k, v in ipairs(JS.Order) do
        for p, q in pairs(JS.Users[v]) do
            if q == 0 then JS.Users[v][p] = nil end
        end
    end
end
        

---------------------
--      Events     --
---------------------

local jS = CreateFrame("Frame")

function JS.ADDON_LOADED(modname)
    if modname == "JuliaStatistics" then
        JoinChannelByName("JStatX")
        if not JSBase then JSBase = {} end
        LoadData()
        for k, v in pairs(JS.Data) do
            if not JS.Users[k] then JS.Users[k] = {} end
            if v ~=0 then JS.Users[k][playerName] =  v end
        end
        jS:RegisterEvent("CHAT_MSG_RAID_BOSS_EMOTE")
        jS:RegisterEvent("CHAT_MSG_ADDON")
        jS:RegisterEvent("CHAT_MSG_CHANNEL")
        jS:RegisterEvent("PLAYER_ENTERING_WORLD")
        jS:RegisterEvent("PLAYER_LEAVING_WORLD")
        ClearData()
        updateItems()
    end
end

function JS.CHAT_MSG_RAID_BOSS_EMOTE(msg)
    for k, v in pairs(L) do
        if v == msg then
            JS.Data[k] = JS.Data[k] + 1
            JS.Users[k] = JS.Users[k] or {}
            JS.Users[k][playerName] = (JS.Users[k][playerName] or 0) + 1
            JS.LastWin[k] = playerName
            UpdateSum(playerName)
            SendChatMessage(k,"CHANNEL", DEFAULT_CHAT_FRAME.editBox.languageID, GetChannelName("JStatX"))
            SendChatMessage(GetHash(JS.Users),"CHANNEL", DEFAULT_CHAT_FRAME.editBox.languageID, GetChannelName("JStatX"))
            updateItems()
            SaveData()
        end
    end
end

function JS.CHAT_MSG_ADDON(prefix, msg, how, who) 
    if prefix == "JstatZ" and L.names[msg] and who ~= playerName then -- для приема сообщений от старых вейсий аддона
        if JS.Options.share then 
            DEFAULT_CHAT_FRAME:AddMessage(who .. L.recieve .. L.names[msg] .. " x".. JS.Amount[msg] .. "!", 0, 0.7, 1)
        end
        JS.LastWin[msg] = who
        JS.Users[msg] = JS.Users[msg] or {}
        JS.Users[msg][who] = (JS.Users[msg][who] or 0) + 1
        UpdateSum(who)
        updateItems()
        SaveData()
    elseif prefix == "JStatReq" then
        if msg == "subhash" then
            for i, j in ipairs(JS.Order) do
                JS.Synced[j] = false
                SendAddonMessage("JStatSH" .. j, GetHash({[j] = JS.Users[j]}), "WHISPER", who)
            end
        else 
            for i,j in pairs(JS.Users[msg]) do
                SendAddonMessage("JSD" .. msg, i .. " " .. j, "WHISPER", who)
            end
            if not JS.Synced[msg] then
                SendAddonMessage("JStatReq", msg , "WHISPER", who)
                JS.Synced[msg] = true
            end
        end
    elseif prefix:sub(1,7) == "JStatSH" then
        local token = prefix:sub(8)
        if GetHash({[token]=JS.Users[token]}) ~= msg then
            SendAddonMessage("JStatReq", token , "WHISPER", who)
        end
    elseif prefix:sub(1,3) == "JSD" then
        local cut = msg:find(" ")
        local token, name, amount = prefix:sub(4), msg:sub(1,cut-1), tonumber(msg:sub(cut+1))
        JS.Users[token] = JS.Users[token] or {}
        if (JS.Users[token][name] or 0) < amount then  JS.Users[token][name] = amount end
        UpdateSum(who)
    end
end

function JS.CHAT_MSG_CHANNEL(msg, who, lang, chanN, tow, flag, zone, num, chan)
    if chan == "JStatX" and L.names[msg] and who ~= playerName then
        if JS.Options.share then
            DEFAULT_CHAT_FRAME:AddMessage(who .. L.recieve .. L.names[msg] .. " x".. JS.Amount[msg] .. "!", 0, 0.7, 1)
        end
        JS.LastWin[msg] = who
        JS.Users[msg] = JS.Users[msg] or {}
        JS.Users[msg][who] = (JS.Users[msg][who] or 0) + 1
        UpdateSum(who)
        updateItems()
        SaveData()
    elseif chan == "JStatX" and msg:len() == 64 and who ~= playerName then
        if GetHash(JS.Users) ~= msg then
            SendAddonMessage("JStatReq", "subhash", "WHISPER", who)
        end
    end
end

function JS.PLAYER_ENTERING_WORLD()
end

function JS.PLAYER_LEAVING_WORLD()
    SaveData()
end

local function mainHandler(self , event , ...)
    return JS[event](...)
end

jS:RegisterEvent("ADDON_LOADED")
jS:SetScript("OnEvent", mainHandler)

---------------------
--    Interface    --
---------------------

local jSGUI = CreateFrame("Frame",nil,UIParent) -- окно аддона
jSGUI:Hide()
jSGUI.isOpen = false
jSGUI:SetFrameStrata("DIALOG")
jSGUI:SetWidth(500)
jSGUI:SetHeight(525)
jSGUI:SetBackdrop(backdrop)
jSGUI:SetBackdropColor(0,0,0,.85)
jSGUI:SetPoint("CENTER",0,0)
jSGUI:SetMovable(true)
jSGUI:EnableMouse(true)
jSGUI:SetScript("OnMouseDown",function()
    jSGUI:StartMoving()
  end)
jSGUI:SetScript("OnMouseUp",function()
    jSGUI:StopMovingOrSizing()
  end)

jSGUI.options = CreateFrame("Frame", nil, jSGUI) -- опции
jSGUI.options:Hide()
jSGUI.options.optionsShown = false
jSGUI.options:SetWidth(494)
jSGUI.options:SetHeight(65)
jSGUI.options:SetPoint("TOP", 0, 65)
jSGUI.options:SetBackdrop(backdrop)
jSGUI.options:SetBackdropColor(0,0,0,.85)

jSGUI.options.shareCB = CreateFrame("CheckButton", "shareCheckButton", jSGUI.options, "OptionsCheckButtonTemplate")
jSGUI.options.shareCB:SetPoint("TOPLEFT", 20, -30)
getglobal(jSGUI.options.shareCB:GetName() .. 'Text'):SetText("|cffffffff" .. L.shareOption)
if JS.Options.share then jSGUI.options.shareCB:SetChecked() end
jSGUI.options.shareCB:SetScript("OnClick", function() JS.Options.share = jSGUI.options.shareCB:GetChecked() end)

jSGUI.titlebar = CreateFrame("Frame", nil, jSGUI) -- фон заголовка
jSGUI.titlebar:ClearAllPoints()
jSGUI.titlebar:SetWidth(494)
jSGUI.titlebar:SetHeight(35)
jSGUI.titlebar:SetPoint("TOP", 0, -3)
jSGUI.titlebar:SetBackdrop(backdrop_noborder)
jSGUI.titlebar:SetBackdropColor(1,1,1,.10)

jSGUI.text = jSGUI:CreateFontString("Status", "LOW", "GameFontNormal") -- заголовок
jSGUI.text:ClearAllPoints()
jSGUI.text:SetPoint("TOPLEFT", 12, -12)
jSGUI.text:SetFontObject(GameFontWhite)
jSGUI.text:SetFont(STANDARD_TEXT_FONT, 16, "OUTLINE")
jSGUI.text:SetText(L.title)
    
jSGUI.closeButton = CreateFrame("Button", nil, jSGUI) -- крестик
jSGUI.closeButton:SetWidth(20)
jSGUI.closeButton:SetHeight(20)
jSGUI.closeButton:SetNormalTexture(makeTexture(jSGUI.closeButton, "Interface\\AddOns\\JuliaStatistics\\img\\close_button_up"))
jSGUI.closeButton:SetPushedTexture(makeTexture(jSGUI.closeButton, "Interface\\AddOns\\JuliaStatistics\\img\\close_button_down"))
jSGUI.closeButton:SetHighlightTexture(makeTexture(jSGUI.closeButton, "Interface\\AddOns\\JuliaStatistics\\img\\button_highlight", "ADD"))
jSGUI.closeButton:SetPoint("TOPRIGHT", -10,-10)
jSGUI.closeButton:SetScript("OnClick", function()
    jSGUI:Hide()
    jSGUI.options:Hide()
    jSGUI.options.optionsShown = false
    jSGUI.isOpen = false
  end)
  
jSGUI.optionsButton = CreateFrame("Button", nil, jSGUI) -- кнопка опций
jSGUI.optionsButton:SetWidth(20)
jSGUI.optionsButton:SetHeight(20)
jSGUI.optionsButton:SetNormalTexture(makeTexture(jSGUI.optionsButton, "Interface\\AddOns\\JuliaStatistics\\img\\settings_up"))
jSGUI.optionsButton:SetPushedTexture(makeTexture(jSGUI.optionsButton, "Interface\\AddOns\\JuliaStatistics\\img\\settings_down"))
jSGUI.optionsButton:SetHighlightTexture(makeTexture(jSGUI.optionsButton, "Interface\\AddOns\\JuliaStatistics\\img\\button_highlight", "ADD"))
jSGUI.optionsButton:SetPoint("TOPRIGHT", -40,-10)
jSGUI.optionsButton:SetScript("OnClick", function()
    if jSGUI.options.optionsShown then
        jSGUI.options:Hide()
        jSGUI.options.optionsShown = false
        JSOptions = JS.Options
    else
        jSGUI.options:Show()
        jSGUI.options.optionsShown = true
    end
  end)
  
jSGUI.syncButton = CreateFrame("Button", nil, jSGUI) -- кнопка sync
jSGUI.syncButton:SetWidth(20)
jSGUI.syncButton:SetHeight(20)
jSGUI.syncButton:SetNormalTexture(makeTexture(jSGUI.syncButton, "Interface\\AddOns\\JuliaStatistics\\img\\sync_up"))
jSGUI.syncButton:SetPushedTexture(makeTexture(jSGUI.syncButton, "Interface\\AddOns\\JuliaStatistics\\img\\sync_down"))
jSGUI.syncButton:SetHighlightTexture(makeTexture(jSGUI.syncButton, "Interface\\AddOns\\JuliaStatistics\\img\\button_highlight", "ADD"))
jSGUI.syncButton:SetPoint("TOPRIGHT", -70,-10)
jSGUI.syncButton:SetScript("OnClick", function()
    SendChatMessage(GetHash(JS.Users),"CHANNEL", DEFAULT_CHAT_FRAME.editBox.languageID, GetChannelName("JStatX"))
  end)
  
jSGUI.thisChar = jSGUI:CreateFontString("Status", "LOW", "GameFontNormal")
jSGUI.thisChar:ClearAllPoints()
jSGUI.thisChar:SetPoint("TOPLEFT", 125, -50)
jSGUI.thisChar:SetFontObject(GameFontWhite)
jSGUI.thisChar:SetFont(STANDARD_TEXT_FONT, 14, "OUTLINE")
jSGUI.thisChar:SetText(L.thisChar)

jSGUI.All = jSGUI:CreateFontString("Status", "LOW", "GameFontNormal")
jSGUI.All:ClearAllPoints()
jSGUI.All:SetPoint("TOPLEFT", 300, -50)
jSGUI.All:SetFontObject(GameFontWhite)
jSGUI.All:SetFont(STANDARD_TEXT_FONT, 14, "OUTLINE")
jSGUI.All:SetText(L.All)

jSGUI.tooltip = CreateFrame("GameTooltip", "boxy", jSGUI, "GameTooltipTemplate")
jSGUI.tooltip:SetBackdrop(backdrop)

jSGUI.itemsThis = {}
jSGUI.itemsAll = {}

for i, v in pairs(JS.Order) do
    jSGUI.itemsThis[i] = CreateFrame("Button", nil, jSGUI)
    jSGUI.itemsThis[i]:SetWidth(25)
    jSGUI.itemsThis[i]:SetHeight(25)
    jSGUI.itemsThis[i]:SetPoint("TOPLEFT", 120,-50-28*i)
    jSGUI.itemsThis[i].texture = jSGUI:CreateTexture()
    jSGUI.itemsThis[i].texture:SetAllPoints(jSGUI.itemsThis[i])
    jSGUI.itemsThis[i].texture:SetTexture(GetItemIcon(JS.IDs[v]))
    jSGUI.itemsThis[i].textAm = jSGUI.itemsThis[i]:CreateFontString("Status", "LOW", "GameFontNormal")
    jSGUI.itemsThis[i].textAm:ClearAllPoints()
    jSGUI.itemsThis[i].textAm:SetPoint("TOPRIGHT", -27, -13)
    jSGUI.itemsThis[i].textAm:SetFontObject(GameFontWhite)
    jSGUI.itemsThis[i].textAm:SetFont(STANDARD_TEXT_FONT, 12, "OUTLINE")
    jSGUI.itemsThis[i].textAm:SetText(JS.Amount[v])
    jSGUI.itemsThis[i].textName = jSGUI.itemsThis[i]:CreateFontString("Status", "LOW", "GameFontNormal")
    jSGUI.itemsThis[i].textName:ClearAllPoints()
    jSGUI.itemsThis[i].textName:SetPoint("TOPRIGHT", -27, 0)
    jSGUI.itemsThis[i].textName:SetFontObject(GameFontWhite)
    jSGUI.itemsThis[i].textName:SetFont(STANDARD_TEXT_FONT, 12, "OUTLINE")
    jSGUI.itemsThis[i].textName:SetText(L.names[v])
    
    jSGUI.itemsThis[i].Amount = jSGUI.itemsThis[i]:CreateFontString("Status", "LOW", "GameFontNormal")
    
    jSGUI.itemsThis[i].AmountP = jSGUI.itemsThis[i]:CreateFontString("Status", "LOW", "GameFontNormal")

    jSGUI.itemsAll[i] = CreateFrame("Button", nil, jSGUI)
    jSGUI.itemsAll[i]:SetWidth(25)
    jSGUI.itemsAll[i]:SetHeight(25)
    jSGUI.itemsAll[i]:SetPoint("TOPLEFT", 300,-50-28*i)
    jSGUI.itemsAll[i].texture = jSGUI:CreateTexture()
    jSGUI.itemsAll[i].texture:SetAllPoints(jSGUI.itemsAll[i])
    jSGUI.itemsAll[i].texture:SetTexture(GetItemIcon(JS.IDs[v]))

    jSGUI.itemsAll[i]:SetScript("OnEnter", function(self, event,...)
    jSGUI.tooltip:SetOwner(self, "ANCHOR_CURSOR")
    jSGUI.tooltip:SetText(L.lastWin .. "|cFF72EAE9" .. JS.LastWin[v].. "|r")
    jSGUI.tooltip:Show()
    end)
    jSGUI.itemsAll[i]:SetScript("OnLeave", function(self, event,...)
    jSGUI.tooltip:Hide()
    end)
    
    jSGUI.itemsAll[i].Amount = jSGUI.itemsAll[i]:CreateFontString("Status", "LOW", "GameFontNormal")
    jSGUI.itemsAll[i].AmountP = jSGUI.itemsAll[i]:CreateFontString("Status", "LOW", "GameFontNormal")
end

function updateItems()
    JS.Data["Gratz"]   = JS.Data["Honor"] + JS.Data["Arena"] + JS.Data["Badge"] + JS.Data["Token"] + JS.Data["Orb"] + JS.Data["LiliRune"]
    JS.Data["Jackpot"] = JS.Data["HonorJ"] + JS.Data["ArenaJ"] + JS.Data["BadgeJ"] + JS.Data["TokenJ"] + JS.Data["OrbJ"] + JS.Data["SirusRune"] + JS.Data["Bd"]
    for i, v in pairs(JS.Order) do
        jSGUI.itemsThis[i].Amount:ClearAllPoints()
        jSGUI.itemsThis[i].Amount:SetPoint("TOPLEFT", 27, 0)
        jSGUI.itemsThis[i].Amount:SetFontObject(GameFontWhite)
        jSGUI.itemsThis[i].Amount:SetFont(STANDARD_TEXT_FONT, 20, "OUTLINE")
        jSGUI.itemsThis[i].Amount:SetText(JS.Data[v])
        
        jSGUI.itemsThis[i].AmountP:ClearAllPoints()
        jSGUI.itemsThis[i].AmountP:SetPoint("TOPLEFT", 70, 0)
        jSGUI.itemsThis[i].AmountP:SetFontObject(GameFontWhite)
        jSGUI.itemsThis[i].AmountP:SetFont(STANDARD_TEXT_FONT, 20, "OUTLINE")
        local strSH
        if v == "Gratz" or v == "Jackpot" then
            local numerator
            if (JS.Data["Gratz"]+JS.Data["Jackpot"]) == 0 then
                numerator = 1
            else 
                numerator = (JS.Data["Gratz"]+JS.Data["Jackpot"])
            end
            strSH = (math.floor((JS.Data[v]/numerator)*1000)/10).."%"
        else
            local numerator
            if summary(JS.Data) == 0 then
                numerator = 1
            else 
                numerator = summary(JS.Data)
            end
            strSH = (math.floor(JS.Data[v]/numerator*1000)/10).."%"
        end
        jSGUI.itemsThis[i].AmountP:SetText(strSH)
        
        jSGUI.itemsAll[i].Amount:ClearAllPoints()
        jSGUI.itemsAll[i].Amount:SetPoint("TOPLEFT", 27, 0)
        jSGUI.itemsAll[i].Amount:SetFontObject(GameFontWhite)
        jSGUI.itemsAll[i].Amount:SetFont(STANDARD_TEXT_FONT, 20, "OUTLINE")
        jSGUI.itemsAll[i].Amount:SetText(summary(JS.Users[v]))
        
        jSGUI.itemsAll[i].AmountP:ClearAllPoints()
        jSGUI.itemsAll[i].AmountP:SetPoint("TOPLEFT", 100, 0)
        jSGUI.itemsAll[i].AmountP:SetFontObject(GameFontWhite)
        jSGUI.itemsAll[i].AmountP:SetFont(STANDARD_TEXT_FONT, 20, "OUTLINE")
        local strSN
        if v == "Gratz" or v == "Jackpot" then
            local numerator
            if (summary(JS.Users["Gratz"]) + summary(JS.Users["Gratz"])) == 0 then
                numerator = 1
            else 
                numerator = summary(JS.Users["Gratz"]) + summary(JS.Users["Jackpot"])
            end
            strSN = (math.floor(summary(JS.Users[v])/numerator*1000)/10).."%"
        else
            local numerator
            if summary(JS.Users) == 0 then
                numerator = 1
            else 
                numerator = summary(JS.Users)
            end
            strSN = (math.floor(summary(JS.Users[v])/numerator*1000)/10).."%"
        end
        jSGUI.itemsAll[i].AmountP:SetText(strSN)
    end
end

---------------------
--  Slash command  --
---------------------

SLASH_JULIASTATISTICS1 = "/jstat"
SlashCmdList["JULIASTATISTICS"] = function()
jSGUI:Show()
end

---------------------
-- Minimap Button  --
---------------------
do
    
    local function moveButton(self)
        local minimapShape = _G.GetMinimapShape and _G.GetMinimapShape() or 'ROUND'
        local centerX, centerY = Minimap:GetCenter()
        local x, y = GetCursorPosition()
        x, y = x / self:GetEffectiveScale() - centerX, y / self:GetEffectiveScale() - centerY
        centerX, centerY = math.abs(x), math.abs(y)
        local cos , sin = centerX / math.sqrt(centerX^2 + centerY^2) , centerY / sqrt(centerX^2 + centerY^2)
        if minimapShape == "ROUND" then
            centerX, centerY = cos*80, sin*80
        elseif minimapShape == "SQUARE" then
                centerX, centerY = math.max(-82, math.min(110*cos, 84)) , math.max(-86, math.min(110*sin, 82))
        end
        centerX = x < 0 and -centerX or centerX
        centerY = y < 0 and -centerY or centerY
        self:ClearAllPoints()
        JS.Options.minimapPos = JS.Options.minimapPos or {}
        JS.Options.minimapPos.x, JS.Options.minimapPos.y  = centerX, centerY
        self:SetPoint("CENTER", centerX, centerY)
    end
    
    jSGUI.button = CreateFrame("Button", "JMinimapButton", Minimap)
    jSGUI.button:SetHeight(32)
    jSGUI.button:SetWidth(32)
    jSGUI.button:SetFrameStrata("MEDIUM")
    jSGUI.button:SetPoint("CENTER", JS.Options.minimapPos.x ,JS.Options.minimapPos.y)
    jSGUI.button:SetMovable(true)
    jSGUI.button:SetUserPlaced(true)
    jSGUI.button:SetNormalTexture("Interface\\AddOns\\JuliaStatistics\\img\\MinimapButtonNormal")
    jSGUI.button:SetPushedTexture("Interface\\AddOns\\JuliaStatistics\\img\\MinimapButtonPushed")
    jSGUI.button:SetHighlightTexture("Interface\\Minimap\\UI-Minimap-ZoomButton-Highlight")
    jSGUI.button:RegisterEvent("PLAYER_ENTERING_WORLD")
    jSGUI.button:SetScript("OnMouseDown", function(self, button)
        if IsShiftKeyDown() then
            self:SetScript("OnUpdate", moveButton)
        end
    end)
    jSGUI.button:SetScript("OnMouseUp", function(self)
            self:SetScript("OnUpdate", nil)
        end)
    jSGUI.button:SetScript("OnClick", function(self, button)
        if jSGUI.isOpen and not IsShiftKeyDown() then
            jSGUI:Hide()
            jSGUI.isOpen = false
        elseif not IsShiftKeyDown() then
            updateItems()
            SaveData()
            jSGUI:Show()
            jSGUI.isOpen = true
        end
    end)
    jSGUI.button:SetScript("OnEvent", function(self, event)
        if event == "PLAYER_ENTERING_WORLD" then
            if JS.Options.minimapIcon then
                jSGUI.options.minimapCB:SetChecked()
                jSGUI.button:Show()
            else 
                jSGUI.button:Hide()
            end
        end
    end)
        
    jSGUI.options.minimapCB = CreateFrame("CheckButton", "minimapIconCheckButton", jSGUI.options, "OptionsCheckButtonTemplate")
    jSGUI.options.minimapCB:SetPoint("TOPLEFT", 20, -10)
    getglobal(jSGUI.options.minimapCB:GetName() .. 'Text'):SetText("|cffffffff" .. L.mIconOption)
    jSGUI.options.minimapCB:SetScript("OnClick", function()
         if jSGUI.options.minimapCB:GetChecked() then
            jSGUI.button:Show()
            JS.Options.minimapIcon = true
         else
            jSGUI.button:Hide()
            JS.Options.minimapIcon = false
         end
        end)

end
