﻿if GetLocale() ~= "ruRU" then return end

L = {
    ["Gratz"]     = "Поздравляем!",
    ["Jackpot"]   = "ДЖЕКПОТ!",
    ["Honor"]     = "Поздравляем! Вы выиграли 100 очков чести!",
    ["Arena"]     = "Поздравляем! Вы выиграли 25 очков арены!",
    ["Badge"]     = "Поздравляем! Вы выиграли Очки доблести x50!",
    ["Token"]     = "Поздравляем! Вы выиграли Жетон воина Запределья x3!",
    ["Orb"]       = "Поздравляем! Вы выиграли Ледяной шар x1!",
    ["LiliRune"]  = "Поздравляем! Вы выиграли Руна Лили x1!",
    ["HonorJ"]    = "|cFFC80046Д|r|cFFC80025Ж|r|cFFC80001Е|r|cFFC82100К|r|cFFC84000П|r|cFFC85C00О|r|cFFC87700Т|r|cFFC79400!|r Вы выиграли 500 очков чести!",
    ["ArenaJ"]    = "|cFFC80046Д|r|cFFC80025Ж|r|cFFC80001Е|r|cFFC82100К|r|cFFC84000П|r|cFFC85C00О|r|cFFC87700Т|r|cFFC79400!|r Вы выиграли 125 очков арены!",
    ["BadgeJ"]    = "|cFFC80046Д|r|cFFC80025Ж|r|cFFC80001Е|r|cFFC82100К|r|cFFC84000П|r|cFFC85C00О|r|cFFC87700Т|r|cFFC79400!|r Вы выиграли Очки доблести x250!",
    ["TokenJ"]    = "|cFFC80046Д|r|cFFC80025Ж|r|cFFC80001Е|r|cFFC82100К|r|cFFC84000П|r|cFFC85C00О|r|cFFC87700Т|r|cFFC79400!|r Вы выиграли Жетон воина Запределья x9!",
    ["OrbJ"]      = "|cFFC80046Д|r|cFFC80025Ж|r|cFFC80001Е|r|cFFC82100К|r|cFFC84000П|r|cFFC85C00О|r|cFFC87700Т|r|cFFC79400!|r Вы выиграли Изначальная Пустота x1!",
    ["SirusRune"] = "|cFFC80046Д|r|cFFC80025Ж|r|cFFC80001Е|r|cFFC82100К|r|cFFC84000П|r|cFFC85C00О|r|cFFC87700Т|r|cFFC79400!|r Вы выиграли Руна Сируса x1!",
    ["Bd"]        = "|cFFC80046Д|r|cFFC80025Ж|r|cFFC80001Е|r|cFFC82100К|r|cFFC84000П|r|cFFC85C00О|r|cFFC87700Т|r|cFFC79400!|r Вы выиграли Черный бриллиант x1!",
    title         = "|cff33ffccСтатистика |cffffffffДжулии",
    thisChar      = "Этот персонаж:",
    All           = "Всего:",
    recieve       = " получил ",
    lastWin       = "Последний выигрыш: ",
    mIconOption   = "Отображать иконку у миникарты",
    shareOption   = "Отображать выигрыши других игроков в чате",
    names = {
    ["Gratz"]     = "Обычные",
    ["Jackpot"]   = "Джекпоты",
    ["Honor"]     = "Очки чести",
    ["Arena"]     = "Очки арены",
    ["Badge"]     = "Очки доблести",
    ["Token"]     = "Жетон",
    ["Orb"]       = "Ледяной шар",
    ["LiliRune"]  = "Руна Лили",
    ["HonorJ"]    = "Очки чести",
    ["ArenaJ"]    = "Очки арены",
    ["BadgeJ"]    = "Очки доблести",
    ["TokenJ"]    = "Жетон",
    ["OrbJ"]      = "Пустота",
    ["SirusRune"] = "Руна Сируса",
    ["Bd"]        = "Бриллиант"
    }
}
